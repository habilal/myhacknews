package app.hack.news;

import java.util.ArrayList;

import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.IApiCallback;
import app.hack.news.models.StoryItem;

import static org.junit.Assert.assertNotNull;

/**
 * Created by macmini on 12/11/16.
 */

public class StoryItemTest {

    private StoryItem storyItem;
    private String id = null;

    @org.junit.Test
    public void getStoryById() {

        NewsApi.getInstance().getTopStroies(new IApiCallback() {
            @Override
            public void onSuccess(Object obj) {
                ArrayList<String> ids = (ArrayList<String>) obj;
                id = ids.get(0);
                NewsApi.getInstance().getStroyItem(id, new IApiCallback() {
                    @Override
                    public void onSuccess(Object obj) {
                        storyItem = (StoryItem) obj;
                        assertNotNull(storyItem);
                    }

                    @Override
                    public void onFail(Object obj) {
                        assertNotNull(storyItem);
                    }
                });

            }

            @Override
            public void onFail(Object obj) {
                assertNotNull(id);
            }
        });
    }
}
