package app.hack.news;

import java.util.ArrayList;

import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.IApiCallback;
import app.hack.news.models.StoryItem;

import static org.junit.Assert.assertNotNull;

/**
 * Created by macmini on 12/11/16.
 */

public class TopStoriesTest {

    private ArrayList<String> topStoriesId = null;
    private StoryItem storyItem;

    @org.junit.Test
    public void getTopStories() {
        NewsApi.getInstance().getTopStroies(new IApiCallback() {
            @Override
            public void onSuccess(Object obj) {
                topStoriesId = (ArrayList<String>) obj;
                assertNotNull(topStoriesId);
            }

            @Override
            public void onFail(Object obj) {
                assertNotNull(topStoriesId);
            }
        });
    }
}
