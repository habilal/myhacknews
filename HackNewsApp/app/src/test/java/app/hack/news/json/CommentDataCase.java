package app.hack.news.json;

import org.junit.Before;

import app.hack.news.api.NewsApi;
import app.hack.news.models.CommentItem;

import static org.junit.Assert.assertEquals;

/**
 * Created by macmini on 12/11/16.
 */

public class CommentDataCase {

    private CommentItem commentItem;
    private final String POSTED_BY = "fowl2";
    private final String ID = "13136879";
    private final String PARENT_ID = "13136426";
    private final String TYPE = "comment";
    private final String TEXT = "Quite professional, well written and clearly the words of someone who cares about the product.<p>Lack of profanities already exceeded the LKML reputation.<p>I think the point about rules being applied consistently is very true. If Alice does the work to comply then Bob shouldn&#x27;t be able to get away without doing it just because he&#x27;s bigger.";
    private final int NUMBER_OF_COMMENTS = 2;

    @Before
    public void setUp() {
        commentItem = NewsApi.getInstance().getGson().fromJson(JSONExample.commentJson, CommentItem.class);
    }

    @org.junit.Test
    public void returnsPostedbyStory() {
        assertEquals(POSTED_BY, commentItem.getBy());
    }

    @org.junit.Test
    public void returnsParentID() {
        assertEquals(PARENT_ID, commentItem.getParent());
    }

    @org.junit.Test
    public void returnsCommentID() {
        assertEquals(ID, commentItem.getId());
    }

    @org.junit.Test
    public void returnsCommentType() {
        assertEquals(TYPE, commentItem.getType());
    }

    @org.junit.Test
    public void returnsCommentText() {
        assertEquals(TEXT, commentItem.getText());
    }

    @org.junit.Test
    public void returnsCommentsKid() {
        assertEquals(NUMBER_OF_COMMENTS, commentItem.getKids().size());
    }

}
