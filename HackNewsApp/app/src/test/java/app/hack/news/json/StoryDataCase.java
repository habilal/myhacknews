package app.hack.news.json;

import org.junit.Before;

import app.hack.news.api.NewsApi;
import app.hack.news.models.StoryItem;

import static org.junit.Assert.assertEquals;

/**
 * Created by macmini on 12/11/16.
 */


public class StoryDataCase {

    private StoryItem storyItem;
    private final String POSTED_BY = "pero";
    private final String DESCENDATS = "108";
    private final String ID = "13136426";
    private final String TYPE = "story";
    private final String TITLE = "Linux kernel maintainer says no to AMDGPU patch";
    private final String SCORES = "310";
    private final int NUMBER_OF_COMMENTS = 15;

    @Before
    public void setUp() {
        storyItem = NewsApi.getInstance().getGson().fromJson(JSONExample.storyJson, StoryItem.class);
    }

    @org.junit.Test
    public void returnsPostedbyStory() {
        assertEquals(POSTED_BY, storyItem.getBy());
    }

    @org.junit.Test
    public void returnsAllDescendants() {
        assertEquals(DESCENDATS, storyItem.getDescendants());
    }

    @org.junit.Test
    public void returnsStoryID() {
        assertEquals(ID, storyItem.getId());
    }

    @org.junit.Test
    public void returnsStroyType() {
        assertEquals(TYPE, storyItem.getType());
    }

    @org.junit.Test
    public void returnsStoryTitle() {
        assertEquals(TITLE, storyItem.getTitle());
    }

    @org.junit.Test
    public void returnsStoryScore() {
        assertEquals(SCORES, storyItem.getScore());
    }

    @org.junit.Test
    public void returnsStoryComments() {
        assertEquals(NUMBER_OF_COMMENTS, storyItem.getKids().size());
    }

}
