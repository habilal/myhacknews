package app.hack.news.json;

/**
 * Created by macmini on 12/9/16.
 */

public interface JSONExample {
    String storyJson = "{\"by\":\"pero\",\"descendants\":108,\"id\":13136426,\"kids\":[13136587,13136879,13136777,13136594,13136531,13136712,13136561,13137473,13137376,13136550,13136755,13136580,13136604,13136596,13136637],\"score\":310,\"time\":1481253712,\"title\":\"Linux kernel maintainer says no to AMDGPU patch\",\"type\":\"story\",\"url\":\"https://lists.freedesktop.org/archives/dri-devel/2016-December/126516.html\"}";
    String commentJson = "{\"by\":\"fowl2\",\"id\":13136879,\"kids\":[13137484,13137568],\"parent\":13136426,\"text\":\"Quite professional, well written and clearly the words of someone who cares about the product.<p>Lack of profanities already exceeded the LKML reputation.<p>I think the point about rules being applied consistently is very true. If Alice does the work to comply then Bob shouldn&#x27;t be able to get away without doing it just because he&#x27;s bigger.\",\"time\":1481261575,\"type\":\"comment\"}";

}
