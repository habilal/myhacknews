package app.hack.news.interfaces;

/**
 * Created by macmini on 12/5/16.
 */

public interface IDialogCallBacks {
    void onOk();
    void onCancel();
}
