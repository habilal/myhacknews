package app.hack.news.api;

import java.util.ArrayList;

import app.hack.news.models.CommentItem;
import app.hack.news.models.StoryItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by macmini on 12/5/16.
 */

public interface IWebservices {

    @GET("topstories.json")
    Call<ArrayList<String>> getTopStories();

    @GET("item/{p1}.json")
    Call<StoryItem> getStoryItem(@Path("p1") String itemId);

    @GET("item/{p1}.json")
    Call<CommentItem> getCommentItem(@Path("p1") String itemId);
}
