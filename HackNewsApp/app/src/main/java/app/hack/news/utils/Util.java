package app.hack.news.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import app.hack.news.R;
import app.hack.news.interfaces.IDialogCallBacks;
import app.hack.news.ui.NewsApplication;

/**
 * Created by macmini on 12/5/16.
 */

public class Util {
    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) NewsApplication.getsInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(final String text) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(NewsApplication.getsInstance(), text, Toast.LENGTH_LONG).show();
            }
        }, 0);

    }

    /**
     * Show Simple Alert Message with OK button. Just Pass title, Message and Callabck on Click
     */
    public static void showAlertMessage(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setCancelable(true).show();
    }

    public static void showAlertMessage(Context context, String title, String message, String okText, final IDialogCallBacks callback) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.onOk();
                        }
                        dialog.dismiss();
                    }
                }).setCancelable(true).show();
    }

    public static void showAlertMessage(Context context, String title, String message, String okText, String cancelText, final IDialogCallBacks callback) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (callback != null) {
                            callback.onOk();
                        }
                        dialog.dismiss();
                    }
                }).setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (callback != null) {
                    callback.onCancel();
                }
                dialog.dismiss();
            }
        }).setCancelable(false).show();
    }
}
