package app.hack.news.interfaces;

/**
 * Created by macmini on 12/5/16.
 */

public interface IApiCallback {
    void onSuccess(Object obj);
    void onFail(Object obj);
}
