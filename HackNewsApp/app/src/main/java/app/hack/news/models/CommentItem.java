package app.hack.news.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macmini on 12/6/16.
 */

public class CommentItem implements ParentListItem {
    private String id;
    private String time;
    private String text;
    private String parent;
    private String by;
    private ArrayList<String> kids;
    private String type;
    private boolean deleted;
    private boolean dead;

    private List<CommentItem> commentItem = new ArrayList<CommentItem>();

    public void setCommentItem(CommentItem commentItem) {
        this.commentItem = new ArrayList<CommentItem>();
        this.commentItem.add(commentItem);
    }

    public String getId() {
        return id;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead(boolean dead) {
        this.dead = dead;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public ArrayList<String> getKids() {
        return kids;
    }

    public void setKids(ArrayList<String> kids) {
        this.kids = kids;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public List<CommentItem> getChildItemList() {
        if (commentItem != null && commentItem.size() == 0) {
            String kidID = (kids != null && kids.size() > 0) ? kids.get(0) : "-1";
            if (!kidID.equalsIgnoreCase("-1")) {
                CommentItem item = new CommentItem();
                item.setId(kidID);
                item.setParent(id);
                commentItem.add(item);
            }
        }
        return commentItem;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
