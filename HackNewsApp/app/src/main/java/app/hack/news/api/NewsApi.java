package app.hack.news.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.hack.news.interfaces.IApiCallback;
import app.hack.news.models.CommentItem;
import app.hack.news.models.StoryItem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by macmini on 12/5/16.
 */

public class NewsApi {

    public static final String BASE_URL = "https://hacker-news.firebaseio.com/v0/";
    public static final int STATUS_OK = 200;

    private static NewsApi newsApi;

    private Retrofit mRetrofit;

    private Gson mGson;
    private IWebservices mApiService;
    private Map<String, Object> mDefaultQueryMap = new HashMap<>();

    private NewsApi() {
        mGson = new GsonBuilder()
                .create();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .build();

        mApiService = mRetrofit.create(IWebservices.class);

    }

    /**
     * Returns the singleton instance of API class
     */
    public static NewsApi getInstance() {
        if (newsApi == null) {
            newsApi = new NewsApi();
        }
        return newsApi;
    }

    public Gson getGson() {
        return mGson;
    }


    public void getTopStroies(final IApiCallback iApiCallback) {

        Call<ArrayList<String>> call = mApiService.getTopStories();
        call.enqueue(new Callback<ArrayList<String>>() {
            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {
                iApiCallback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                iApiCallback.onFail(t.getMessage());
            }
        });
    }

    public void getStroyItem(final String itemId, final IApiCallback iApiCallback) {

        Call<StoryItem> call = mApiService.getStoryItem(itemId);
        call.enqueue(new Callback<StoryItem>() {
            @Override
            public void onResponse(Call<StoryItem> call, Response<StoryItem> response) {
                    iApiCallback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<StoryItem> call, Throwable t) {
                iApiCallback.onFail(itemId);
            }
        });
    }

    public void getCommentItem(final String itemId, final IApiCallback iApiCallback) {

        Call<CommentItem> call = mApiService.getCommentItem(itemId);
        call.enqueue(new Callback<CommentItem>() {
            @Override
            public void onResponse(Call<CommentItem> call, Response<CommentItem> response) {
                iApiCallback.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<CommentItem> call, Throwable t) {
                iApiCallback.onFail(itemId);
            }
        });
    }
}
