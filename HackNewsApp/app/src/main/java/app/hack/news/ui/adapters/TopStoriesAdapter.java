package app.hack.news.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.hack.news.R;
import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.OnLoadMoreListener;
import app.hack.news.managers.ApplicationState;
import app.hack.news.models.StoryItem;
import app.hack.news.ui.activities.CommentActivity;
import app.hack.news.utils.Constants;

/**
 * Created by macmini on 12/5/16.
 */

public class TopStoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> mData;
    private String TAG = TopStoriesAdapter.class.getName();

    public final int TYPE_MASTERDATA = 0;
    public final int TYPE_LOAD = 1;
    private Context context;
    private boolean isPullTorefresh = false;

    OnLoadMoreListener loadMoreListener;

    public TopStoriesAdapter(Context context, List<String> recentModels) {
        mData = recentModels;
        this.context = context;
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mData.get(position).equalsIgnoreCase(Constants.LOAD_MORE_STRING)) {
            return TYPE_LOAD;
        } else {
            return TYPE_MASTERDATA;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        if (viewType == TYPE_MASTERDATA) {
            return new MasterViewHolder(inflater.inflate(R.layout.topstory_list_items, viewGroup, false));
        } else {
            return new LoadHolder(inflater.inflate(R.layout.loadmore_item, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_MASTERDATA) {
            ((MasterViewHolder) holder).bindview(position);
        }else{
            ((LoadHolder) holder).bindview(position);
        }
    }


    public class MasterViewHolder extends RecyclerView.ViewHolder {
        TextView title , discription, count;

        public MasterViewHolder(View itemView) {
            super(itemView);
            count = (TextView) itemView.findViewById(R.id.count);
            title = (TextView) itemView.findViewById(R.id.newTitle);
            discription = (TextView) itemView.findViewById(R.id.discription);
        }

        void bindview(final int i) {
            String itemId = mData.get(i);
            StoryItem storyItem = ApplicationState.getInstance().getTopStories().get(itemId);
            if (storyItem != null) {
                count.setText((i+1) +"");
                title.setText(storyItem.getTitle());
                String comments = Constants.EMPTY_STRING;
                if(storyItem.getKids() != null)
                {
                    comments =  " | "+ storyItem.getKids().size() + " comments";
                }
                CharSequence time = DateUtils.getRelativeTimeSpanString(storyItem.getTime() * 1000, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);

                discription.setText(storyItem.getScore() +" points by "+ storyItem.getBy() +" "+time+  comments);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ApplicationState.getInstance().setStoryComments(new ArrayList<String>());
                        StoryItem storyItem = (StoryItem) itemView.getTag();
                        Intent intent = new Intent(context, CommentActivity.class);
                        intent.putExtra(Constants.SELECTED_ITEM, NewsApi.getInstance().getGson().toJson(storyItem));
                        intent.putExtra(Constants.INDEX, i+1);
                        ApplicationState.getInstance().setCommentLoadMoreAvailable(true);
                        context.startActivity(intent);
                    }
                });
                itemView.setTag(storyItem);
            }
        }
    }

    public class LoadHolder extends RecyclerView.ViewHolder {
        public LoadHolder(View itemView) {
            super(itemView);
        }

        void bindview(final int i) {
            String itemId = mData.get(i);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(loadMoreListener != null){
                        loadMoreListener.onLoadMore();
                    }
                }
            });
        }
    }

    public void updateDataSet(List<String> data, int previousCount){
        int size = mData.size();
        if(size != 0) {
            mData.remove(size - 1);
        }
        if(isPullTorefresh){
            mData.clear();
            isPullTorefresh = false;
        }

        if(previousCount == 0)
            mData.clear();

        mData.addAll(data);
        if(ApplicationState.getInstance().isLoadMoreAvailable()) {
            mData.add(Constants.LOAD_MORE_STRING);
        }
        notifyDataSetChanged();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public boolean isPullTorefresh() {
        return isPullTorefresh;
    }

    public void setPullTorefresh(boolean pullTorefresh) {
        isPullTorefresh = pullTorefresh;
    }
}
