package app.hack.news.interfaces;

/**
 * Created by macmini on 12/6/16.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
