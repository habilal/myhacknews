package app.hack.news.utils;

/**
 * Created by macmini on 12/6/16.
 */

public interface Constants {
    String EMPTY_STRING = "";
    String LOAD_MORE_STRING = "loadmore";

    String SELECTED_ITEM = "slectedItem";

    String INDEX = "indexId";
}
