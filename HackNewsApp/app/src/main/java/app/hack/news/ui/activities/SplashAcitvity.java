package app.hack.news.ui.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import app.hack.news.R;
import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.IApiCallback;
import app.hack.news.interfaces.IDialogCallBacks;
import app.hack.news.managers.ApplicationState;
import app.hack.news.utils.Util;

public class SplashAcitvity extends AppCompatActivity {

    private Resources resources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        resources = getResources();
        if(Util.isNetworkAvailable()) {

            NewsApi.getInstance().getTopStroies(new IApiCallback() {
                @Override
                public void onSuccess(Object obj) {
                    ApplicationState.getInstance().setTopStories((ArrayList<String>) obj);
                    continueHomeScreen();
                }

                @Override
                public void onFail(Object obj) {

                }
            });
        }else{
            showNetworkAlert();
        }
    }

    private void showNetworkAlert(){
        Util.showAlertMessage(SplashAcitvity.this,
                resources.getString(R.string.app_name),
                resources.getString(R.string.network_error),
                resources.getResourceName(R.string.ok),new IDialogCallBacks() {
                    @Override
                    public void onOk() {
                        finish();
                    }

                    @Override
                    public void onCancel() {

                    }
                });
    }

    private void continueHomeScreen(){
        Intent intent = new Intent(SplashAcitvity.this, TopStoryActivity.class);
        startActivity(intent);
        finish();
    }
}
