package app.hack.news.ui.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import app.hack.news.R;
import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.IApiCallback;
import app.hack.news.interfaces.OnLoadMoreListener;
import app.hack.news.managers.ApplicationState;
import app.hack.news.models.StoryItem;
import app.hack.news.ui.adapters.TopStoriesAdapter;
import app.hack.news.ui.views.HNDividerItemDecoration;
import app.hack.news.utils.Util;

public class TopStoryActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    private TopStoriesAdapter adapter;
    private RelativeLayout progresBar;

    private static int nextcount = 8;
    private static int previousCount = 0;
    private final int increment = 8;
    private List<String> tempList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topstories);

        int dp8 = getResources().getDimensionPixelSize(R.dimen.dp8);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle("Top Stories");
        }

        progresBar =  (RelativeLayout) findViewById(R.id.progressbar);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        recyclerView = (RecyclerView) findViewById(R.id.storyRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new HNDividerItemDecoration(dp8,dp8));

        setUpItemsAdapter();
    }


    @Override
    protected void onResume() {
        super.onResume();
        previousCount = 0;
        if(adapter == null) {
            setUpItemsAdapter();
        }
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeContainer.setRefreshing(true);
                adapter.setPullTorefresh(true);
                fetchRefreshNews();
            }
        });
        if(ApplicationState.getInstance().getTopStories().size() > 0){
            updateItemstempList();
        }else{
            fetchRefreshNews();
        }
    }

    private void fetchRefreshNews(){
        if(Util.isNetworkAvailable())
        {
            NewsApi.getInstance().getTopStroies(new IApiCallback() {
                @Override
                public void onSuccess(Object obj) {
                    ApplicationState.getInstance().setTopStories((ArrayList<String>) obj);
                    previousCount = 0;
                    nextcount = 8;
                    updateItemstempList();
                }

                @Override
                public void onFail(Object obj) {

                }
            });
        }else{
            Util.showToast(getResources().getString(R.string.network_error));
        }
    }
    private void setUpItemsAdapter(){
        adapter = new TopStoriesAdapter(this,new ArrayList<String>());
        recyclerView.setAdapter(adapter);
        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                progresBar.setVisibility(View.VISIBLE);
                previousCount = nextcount;
                nextcount = nextcount + increment;
                updateItemstempList();
            }
        });
    }

    private IApiCallback iApiCallback = new IApiCallback() {
        @Override
        public void onSuccess(Object obj) {
            ApplicationState.getInstance().updateStoryItem((StoryItem) obj);
            getTopStoryItems();
        }

        @Override
        public void onFail(Object obj) {
            NewsApi.getInstance().getStroyItem((String) obj, iApiCallback);
        }
    };

    private void updateItemstempList(){
        tempList = ApplicationState.getInstance().getSubListTopStories(previousCount , nextcount);
        getTopStoryItems();
    }

    private void getTopStoryItems(){
        if(tempList.size() != 0) {
            if(!ApplicationState.getInstance().isContainsModel(tempList.get(0))) {
                NewsApi.getInstance().getStroyItem(tempList.remove(0), iApiCallback);
            }else{
                tempList.remove(0);
                getTopStoryItems();
            }
        }else{
            swipeContainer.setRefreshing(false);
            progresBar.setVisibility(View.GONE);
            adapter.updateDataSet(ApplicationState.getInstance().getSubListTopStories(previousCount,nextcount), previousCount);
        }
    }
}
