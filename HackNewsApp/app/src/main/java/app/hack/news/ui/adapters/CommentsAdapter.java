package app.hack.news.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.Collections;
import java.util.List;

import app.hack.news.R;
import app.hack.news.interfaces.IChildApiListeners;
import app.hack.news.interfaces.OnLoadMoreListener;
import app.hack.news.managers.ApplicationState;
import app.hack.news.models.CommentItem;
import app.hack.news.utils.Constants;

/**
 * Created by macmini on 12/6/16.
 */

public class CommentsAdapter extends ExpandableRecyclerAdapter<CommentsAdapter.CommentParentViewHolder, CommentsAdapter.CommentChildViewHolder> {

    private LayoutInflater mInflater;
    private List<CommentItem> commentItem;
    private OnLoadMoreListener onLoadMoreListener;
    private IChildApiListeners childApiListeners;

    public CommentsAdapter(Context context, @NonNull List<CommentItem> items, OnLoadMoreListener clickListener, IChildApiListeners childApiListeners) {
        super(items);
        commentItem = items;
        mInflater = LayoutInflater.from(context);
        this.onLoadMoreListener = clickListener;
        this.childApiListeners = childApiListeners;
    }

    @Override
    public CommentParentViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View parentView = mInflater.inflate(R.layout.comment_parent_view, parentViewGroup, false);
        return new CommentParentViewHolder(parentView);
    }

    @Override
    public CommentChildViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View childView = mInflater.inflate(R.layout.comment_child_layout, childViewGroup, false);
        return new CommentChildViewHolder(childView);
    }

    @Override
    public void onBindParentViewHolder(CommentParentViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        parentViewHolder.bind((CommentItem) parentListItem);
    }

    @Override
    public void onBindChildViewHolder(CommentChildViewHolder childViewHolder, int position, Object childListItem) {
        childViewHolder.bind(ApplicationState.getInstance().getChildItem(((CommentItem) childListItem).getParent()));
    }

    public class CommentParentViewHolder extends ParentViewHolder {
        private TextView title, commentDescription;
        private LinearLayout loadMore;

        public CommentParentViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.commentName);
            commentDescription = (TextView) itemView.findViewById(R.id.commentDescription);
            loadMore = (LinearLayout) itemView.findViewById(R.id.loadMore);
        }

        private void resetFilds() {
            title.setText("");
            commentDescription.setText("");
        }

        public void bind(@NonNull CommentItem item) {
            if (item != null) {
                if (item.getText() == null)
                    item = ApplicationState.getInstance().getParentItem(item.getId());

                if (!item.isDeleted() && !item.isDead())
                {
                    if (!item.getText().equals(Constants.LOAD_MORE_STRING)) {
                        loadMore.setVisibility(View.GONE);
                        title.setText(item.getBy());
                        commentDescription.setText(Html.fromHtml(item.getText()));
                    } else if (item.getText().equals(Constants.LOAD_MORE_STRING)) {
                        resetFilds();
                        loadMore.setVisibility(View.VISIBLE);
                        loadMore.setTag(item.getId());
                        loadMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                            }
                        });
                    } else {
                        resetFilds();
                    }
                } else {
                    resetFilds();
                    if (item.isDeleted() || item.isDead())
                        loadMore.setVisibility(View.GONE);
                }
            } else {
                resetFilds();
            }
        }

        @Override
        public void onExpansionToggled(boolean expanded) {
            super.onExpansionToggled(expanded);
        }
    }

    public class CommentChildViewHolder extends ChildViewHolder {
        private TextView title, commentDescription;
        private LinearLayout progressbar;

        public CommentChildViewHolder(@NonNull View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.commentName);
            commentDescription = (TextView) itemView.findViewById(R.id.commentDescription);
            progressbar = (LinearLayout) itemView.findViewById(R.id.progressbar);

        }

        private void resetFilds() {
            title.setText("");
            commentDescription.setText("");
        }

        public void bind(@NonNull CommentItem item) {
            if (item != null) {
                if (item.getText() == null) {
                    item = ApplicationState.getInstance().getChildItem(item.getParent());
                }

                if (item.getText() != null) {
                    progressbar.setVisibility(View.GONE);
                    title.setText(item.getBy());
                    commentDescription.setText(Html.fromHtml(item.getText()));
                } else {
                    resetFilds();
                    progressbar.setVisibility(View.VISIBLE);
                    childApiListeners.onChildApiListeners(item.getId());
                }
            } else {
                resetFilds();
            }
        }
    }

    public void addItem(List<CommentItem> data, CommentItem item) {
        data.removeAll(Collections.singleton(null));
        List<CommentItem> list = (List<CommentItem>) getParentItemList();
        int size = list.size();
        if (ApplicationState.getInstance().isCommentLoadMoreAvailable()) {
            data.add(item);
        }
        list.addAll(data);
        notifyParentItemRangeInserted(size, data.size());
    }

    public void removeItem(CommentItem item) {
        List<CommentItem> list = (List<CommentItem>) getParentItemList();
        int index = list.indexOf(item);
        list.remove(index);
        notifyParentItemRemoved(index);

    }
}
