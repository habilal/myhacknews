package app.hack.news.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.hack.news.R;
import app.hack.news.api.NewsApi;
import app.hack.news.interfaces.IApiCallback;
import app.hack.news.interfaces.IChildApiListeners;
import app.hack.news.interfaces.OnLoadMoreListener;
import app.hack.news.managers.ApplicationState;
import app.hack.news.models.CommentItem;
import app.hack.news.models.StoryItem;
import app.hack.news.ui.adapters.CommentsAdapter;
import app.hack.news.ui.views.HNDividerItemDecoration;
import app.hack.news.utils.Constants;

public class CommentActivity extends AppCompatActivity {

    private TextView itemTitle, postedby,commentCount;
    private RelativeLayout progresBar;
    private RecyclerView recyclerView;

    private List<String> tempList = new ArrayList<String>();

    private CommentsAdapter adapter;
    private StoryItem storyItem;
    private ArrayList<String> processingIds = new ArrayList<String>();

    private int nextcount = 5;
    private int previousCount = 0;
    private final int increment = 8;
    private CommentItem loadMoreItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        previousCount = 0;
        String data = getIntent().getStringExtra(Constants.SELECTED_ITEM);
        int id = getIntent().getIntExtra(Constants.INDEX,1);
        int dp8 = getResources().getDimensionPixelSize(R.dimen.dp8);

        if(data == null || data.isEmpty())
            finish();

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("TopStory # "+id);
        }

        storyItem = NewsApi.getInstance().getGson().fromJson(data, StoryItem.class);

        loadMoreItem = new CommentItem();
        loadMoreItem.setText(Constants.LOAD_MORE_STRING);

        itemTitle = (TextView) findViewById(R.id.itemTittle);
        postedby = (TextView) findViewById(R.id.postedby);
        commentCount = (TextView) findViewById(R.id.commentCount);
        progresBar =  (RelativeLayout) findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) findViewById(R.id.commentRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new HNDividerItemDecoration(dp8,dp8));

        itemTitle.setText(storyItem.getTitle());
        postedby.setText("Posted by "+ storyItem.getBy());
        if (storyItem != null && storyItem.getKids() != null && storyItem.getKids().size() > 0) {
            commentCount.setText(storyItem.getKids().size() + " Comments");
        }


        if(ApplicationState.getInstance().getStoryComments().size() == 0 ) {
            if (storyItem != null && storyItem.getKids() != null && storyItem.getKids().size() > 0) {
                ApplicationState.getInstance().setStoryComments(storyItem.getKids());
            } else {
                ApplicationState.getInstance().setStoryComments(new ArrayList<String>());
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpItemsAdapter();
        updateItemstempList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void updateItemstempList(){
        tempList = ApplicationState.getInstance().getSubListCommentIds(previousCount , nextcount);
        getCommentItems();
    }

    private void getCommentItems(){
        if(tempList.size() != 0) {
            if(!ApplicationState.getInstance().isContainsCommentModel(tempList.get(0))) {
                NewsApi.getInstance().getCommentItem(tempList.remove(0), parentCallBack);
            }else{
                tempList.remove(0);
                getCommentItems();
            }
        }else{
            progresBar.setVisibility(View.GONE);
            adapter.addItem(ApplicationState.getInstance().getSubListCommentItems(previousCount,nextcount), loadMoreItem);
        }
    }

    private void setUpItemsAdapter(){
        adapter = new CommentsAdapter(this, new ArrayList<CommentItem>(), new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                adapter.removeItem(loadMoreItem);
                progresBar.setVisibility(View.VISIBLE);
                previousCount = nextcount;
                nextcount = nextcount + increment;
                updateItemstempList();
            }
        }, iChildApiListeners);
        recyclerView.setAdapter(adapter);
    }

    private IApiCallback parentCallBack = new IApiCallback() {
        @Override
        public void onSuccess(Object obj) {
            CommentItem item = (CommentItem) obj;
            if(!item.isDeleted() && !item.isDead()) {
                ApplicationState.getInstance().updateCommentItem(item);
            }else{
                ApplicationState.getInstance().deletedCommentHistory(item.getId());
            }
            getCommentItems();
        }

        @Override
        public void onFail(Object obj) {
            NewsApi.getInstance().getCommentItem((String) obj, parentCallBack);
        }
    };

    private IChildApiListeners iChildApiListeners = new IChildApiListeners() {
        @Override
        public void onChildApiListeners(String id) {
            if(!processingIds.contains(id)) {
                processingIds.add(id);
                NewsApi.getInstance().getCommentItem(id, childCallBack);
            }
        }
    };

    private IApiCallback childCallBack = new IApiCallback() {
        @Override
        public void onSuccess(Object obj) {
            processingIds.remove(((CommentItem) obj).getId());
            ApplicationState.getInstance().updateChildItem((CommentItem) obj);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }

        @Override
        public void onFail(Object obj) {
            NewsApi.getInstance().getCommentItem((String) obj, childCallBack);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
