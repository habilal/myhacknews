package app.hack.news.ui;

import android.app.Application;

/**
 * Created by macmini on 12/5/16.
 */

public class NewsApplication extends Application {

    private static NewsApplication sInstance;

    public static NewsApplication getsInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }
}
