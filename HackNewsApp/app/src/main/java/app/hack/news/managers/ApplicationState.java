package app.hack.news.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import app.hack.news.models.CommentItem;
import app.hack.news.models.StoryItem;

/**
 * Created by macmini on 12/5/16.
 */

public class ApplicationState {

    private static ApplicationState sApplicationState;
    private HashMap<String, StoryItem> topStories;
    private HashMap<String, CommentItem> commentItem;
    private boolean isLoadMoreAvailable = true;
    public boolean isCommentLoadMoreAvailable = true;

    public boolean isLoadMoreAvailable() {
        return isLoadMoreAvailable;
    }


    public void setLoadMoreAvailable(boolean loadMoreAvailable) {
        isLoadMoreAvailable = loadMoreAvailable;
    }

    public boolean isCommentLoadMoreAvailable() {
        return isCommentLoadMoreAvailable;
    }

    public void setCommentLoadMoreAvailable(boolean commentLoadMoreAvailable) {
        isCommentLoadMoreAvailable = commentLoadMoreAvailable;
    }

    private ApplicationState() {
        topStories = new HashMap<String, StoryItem>();
        commentItem = new HashMap<String, CommentItem>();
    }

    public static ApplicationState getInstance() {
        if (sApplicationState == null) {
            sApplicationState = new ApplicationState();
        }
        return sApplicationState;
    }

    public ArrayList<String> getListTopStories() {
        if (topStories == null)
            topStories = new HashMap<String, StoryItem>();
        return new ArrayList<String>(topStories.keySet());
    }

    public List<String> getSubListTopStories(int previous, int next) {
        ArrayList<String> topList = getListTopStories();
        if (topList.size() == 1) {
            next = 1;
        } else if (next > topList.size()) {
            next = topList.size() - 1;
        }

        if (previous > next)
            previous = next;

        if (previous == next) {
            setLoadMoreAvailable(false);
            return new ArrayList<String>();
        } else {
            return topList.subList(previous, next);
        }
    }

    public HashMap<String, StoryItem> getTopStories() {
        if (topStories == null)
            topStories = new HashMap<String, StoryItem>();
        return topStories;
    }

    public void setTopStories(ArrayList<String> tStrories) {
        if (topStories == null) {
            topStories = new HashMap<String, StoryItem>();
        }
        topStories.clear();

        for (String ts : tStrories) {
            topStories.put(ts, null);
        }
    }

    public void updateStoryItem(StoryItem item) {
        if (topStories.containsKey(item.getId())) {
            topStories.put(item.getId(), item);
        }
    }

    public boolean isContainsModel(String id) {
        boolean isAvailable = false;
        if (topStories != null && topStories.containsKey(id)) {
            StoryItem storyItem = topStories.get(id);
            if (storyItem != null) {
                isAvailable = true;
            }
        }
        return isAvailable;
    }

    public boolean isContainsCommentModel(String id) {
        boolean isAvailable = false;
        if (commentItem != null && commentItem.containsKey(id)) {
            CommentItem item = commentItem.get(id);
            if (item != null) {
                isAvailable = true;
            }
        }
        return isAvailable;
    }

    public void updateCommentItem(CommentItem item) {
        if (commentItem.containsKey(item.getId())) {
            commentItem.put(item.getId(), item);
        }
    }

    public void deletedCommentHistory(String id){
        if (commentItem.containsKey(id)) {
            commentItem.remove(id);
        }
    }

    public void updateChildItem(CommentItem commentItem) {
        if (this.commentItem.containsKey(commentItem.getParent())) {
            this.commentItem.get(commentItem.getParent()).setCommentItem(commentItem);
        }
    }

    public CommentItem getParentItem(String id) {
        if (id != null && commentItem.containsKey(id)) {
            return commentItem.get(id);
        }

        return null;
    }

    public CommentItem getChildItem(String id) {
        if (commentItem.containsKey(id) && commentItem.get(id).getChildItemList() != null && commentItem.get(id).getChildItemList().size() > 0)
            return commentItem.get(id).getChildItemList().get(0);

        return null;
    }

    public void setStoryComments(ArrayList<String> tStrories) {
        if (commentItem == null) {
            commentItem = new HashMap<String, CommentItem>();
        }
        commentItem.clear();

        for (String ts : tStrories) {
            commentItem.put(ts, null);
        }
    }

    public ArrayList<CommentItem> getStoryComments() {
        if (commentItem == null)
            commentItem = new HashMap<String, CommentItem>();
        return new ArrayList<CommentItem>(commentItem.values());
    }

    public ArrayList<String> getCommentIds() {
        if (commentItem == null)
            commentItem = new HashMap<String, CommentItem>();
        return new ArrayList<String>(commentItem.keySet());
    }

    public List<String> getSubListCommentIds(int previous, int next) {
        ArrayList<String> commentIds = getCommentIds();

        if (commentIds.size() == 1) {
            next = 1;
        } else if (next > commentIds.size()) {
            next = commentIds.size() - 1;
        }

        if (previous > next)
            previous = next;

        if (previous == next) {
            isCommentLoadMoreAvailable = false;
            return new ArrayList<String>();
        } else {
            return commentIds.subList(previous, next);
        }
    }

    public List<CommentItem> getSubListCommentItems(int previous, int next) {
        ArrayList<CommentItem> commentItems = getStoryComments();
        if (commentItems.size() == 1) {
            next = 1;
        } else if (next > commentItems.size()) {
            next = commentItems.size() - 1;
        }

        if (previous > next)
            previous = next;

        if (previous == next) {
            isCommentLoadMoreAvailable = false;
            return new ArrayList<CommentItem>();
        } else {
            return commentItems.subList(previous, next);
        }
    }
}
